﻿namespace soru1
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.lst = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbadet = new System.Windows.Forms.ComboBox();
            this.btnhesapla = new System.Windows.Forms.Button();
            this.txbsayi1 = new System.Windows.Forms.TextBox();
            this.txbsayi2 = new System.Windows.Forms.TextBox();
            this.lblsonuc = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lst
            // 
            this.lst.FormattingEnabled = true;
            this.lst.Location = new System.Drawing.Point(48, 114);
            this.lst.Name = "lst";
            this.lst.Size = new System.Drawing.Size(120, 95);
            this.lst.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(198, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "adet;";
            // 
            // cmbadet
            // 
            this.cmbadet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbadet.FormattingEnabled = true;
            this.cmbadet.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cmbadet.Location = new System.Drawing.Point(201, 77);
            this.cmbadet.Name = "cmbadet";
            this.cmbadet.Size = new System.Drawing.Size(121, 21);
            this.cmbadet.TabIndex = 4;
            // 
            // btnhesapla
            // 
            this.btnhesapla.Location = new System.Drawing.Point(73, 215);
            this.btnhesapla.Name = "btnhesapla";
            this.btnhesapla.Size = new System.Drawing.Size(75, 23);
            this.btnhesapla.TabIndex = 5;
            this.btnhesapla.Text = "hesapla";
            this.btnhesapla.UseVisualStyleBackColor = true;
            this.btnhesapla.Click += new System.EventHandler(this.Btnhesapla_Click);
            // 
            // txbsayi1
            // 
            this.txbsayi1.Location = new System.Drawing.Point(48, 20);
            this.txbsayi1.Name = "txbsayi1";
            this.txbsayi1.Size = new System.Drawing.Size(100, 20);
            this.txbsayi1.TabIndex = 6;
            // 
            // txbsayi2
            // 
            this.txbsayi2.Location = new System.Drawing.Point(48, 54);
            this.txbsayi2.Name = "txbsayi2";
            this.txbsayi2.Size = new System.Drawing.Size(100, 20);
            this.txbsayi2.TabIndex = 7;
            // 
            // lblsonuc
            // 
            this.lblsonuc.AutoSize = true;
            this.lblsonuc.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblsonuc.Location = new System.Drawing.Point(198, 175);
            this.lblsonuc.Name = "lblsonuc";
            this.lblsonuc.Size = new System.Drawing.Size(81, 26);
            this.lblsonuc.TabIndex = 8;
            this.lblsonuc.Text = "toplam";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 329);
            this.Controls.Add(this.lblsonuc);
            this.Controls.Add(this.txbsayi2);
            this.Controls.Add(this.txbsayi1);
            this.Controls.Add(this.btnhesapla);
            this.Controls.Add(this.cmbadet);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lst);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lst;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbadet;
        private System.Windows.Forms.Button btnhesapla;
        private System.Windows.Forms.TextBox txbsayi1;
        private System.Windows.Forms.TextBox txbsayi2;
        private System.Windows.Forms.Label lblsonuc;
    }
}

